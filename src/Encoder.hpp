#ifndef ENCODER_HPP
#define ENCODER_HPP

#include <mariadb++/connection.hpp>
#include <mariadb++/result_set.hpp>

#include "Maps.hpp"

class Encoder : public std::vector<Maps> {
    public:
        /** default constructor */
        Encoder()=delete;
        Encoder(const std::string &database, const std::string &encoder_table, const mariadb::connection_ref &con) {
            // open database 
            con->execute("USE " + database);
            // obtain content of table encoder 
            mariadb::result_set_ref result = con->query("SELECT * FROM " + encoder_table);
            // reading from a result set
            std::string part_name; /**< This variable contains the name of an attribute in current row */
            std::string attribute_value; /**< This variable saves the value in current row */
            char value_letter; /**< This variable saves the letter in current row */
            std::size_t num_bits; /**< This variable saves the length of the bitset in current row */
            unsigned long bitset_ulong; /**< This variable saves the bitset in current row */
            std::size_t index = 0;
            while (result->next()) {
                part_name = result->get_string("attribute_name");
                // find index of Maps with name part_name
                index = this->findIndex(part_name);
//                std::vector<Maps>::iterator it = std::find_if(this->begin(), this->end(), [&part_name](const Maps &arg) {return arg.getAttributeName() == part_name; });
                attribute_value = result->get_string("attribute_value");
                value_letter = result->get_string("value_letter")[0];
                num_bits = result->get_signed32("bitset_length");
                bitset_ulong = result->get_signed32("value_bitset");
                if(index == this->size()) {
                    this->emplace_back(part_name, attribute_value, value_letter, num_bits, bitset_ulong);
                } else {
                    this->at(index).AddValue(attribute_value, value_letter, num_bits, bitset_ulong);
                }
            }
        }
    private:
        std::size_t findIndex(std::string &part_name) {
            std::size_t index = this->size();
            for (std::size_t ndx = 0; ndx < index; ndx++) {
                if (this->at(ndx).getAttributeName() == part_name) {
                    index = ndx;
                    break;
                }
            }
            return index;
        }
};

#endif // ENCODER_HPP
