#ifndef DATAIMPORT_HPP
#define DATAIMPORT_HPP

#define DEFAULT_DB "mushroom"
#define DEFAULT_HOST "localhost"
#define DEFAULT_PASS "toor"
#define DEFAULT_USER "root"

#include "Encoder.hpp" // also adds all needed headers

namespace vkfencoder{
class DataImport {
    public:
        /** default constructor */
        DataImport() = default;
        /** copy-constructor */
        DataImport(const DataImport&) = delete;
        /** move-constructor */
        DataImport(DataImport&&) = default;
        /** copy-operator= */
        DataImport& operator=(const DataImport& rhs) = delete;
        /** move-operator= */
        DataImport& operator=(DataImport&& rhs) = default;
        /** Constructor Sample from file implementation */
        DataImport(const std::string &input_file_name,
                const char &goal_value,
                const std::string &encoder_table_name,
                const std::string &data_table_name,
                const std::string &database = DEFAULT_DB,
                const std::string &url = DEFAULT_HOST,
                const std::string &user = DEFAULT_USER,
                const std::string &password = DEFAULT_PASS
                ) {            
            // set up the account
            mariadb::account_ref acc = mariadb::account::create(url, user, password);
            // create connection
            mariadb::connection_ref con = mariadb::connection::create(acc);
            // create connection
            const Encoder encoder(database, encoder_table_name, con);
            std::ifstream input_file(input_file_name);
            std::string input_file_line;
            // create table sample
            con->execute("USE " + database);
            con->execute("DROP TABLE IF EXISTS " + data_table_name);
            std::string sql_query("CREATE TABLE IF NOT EXISTS " + data_table_name);
            sql_query.append(" (row_id int(8) NOT NULL AUTO_INCREMENT, ");
            sql_query.append("real_sign tinyint(1) NOT NULL DEFAULT 1, ");
            std::string insert_query("INSERT INTO " + data_table_name + " VALUES (NULL, ?");
            for (auto maps : encoder) {
                sql_query.append(maps.getAttributeName());
                sql_query.append(" varchar(255) NOT NULL DEFAULT 'null', ");
                insert_query.append(", ?");
            }
            sql_query.append("PRIMARY KEY(row_id)) ");
            sql_query.append("ENGINE = InnoDB");
            insert_query.append(")");
            con->execute(sql_query);
            mariadb::statement_ref stmt = con->create_statement(insert_query);
            while (!input_file.eof()) {
                std::getline(input_file,input_file_line);
                if (!input_file_line.empty()) {
                    std::string attribute_name;
                    std::string attribute_value;
                    std::string substring;
                    std::stringstream input_string(input_file_line);
                    std::getline(input_string, substring, ',');
                    char value_letter = substring[0];
                    stmt->set_boolean(0, value_letter == goal_value);
                    std::uint32_t ndx = 0;
                    while (input_string.good()) {
                        std::getline(input_string, substring, ',');
                        if(substring.size() == 1) {
                            char value_letter = substring[0];
                            boost::dynamic_bitset<> encoded_bs = encoder[ndx].findEncoding(value_letter);
                            stmt->set_string(ndx+1, encoder[ndx].findValueName(encoded_bs));
                        }
                        ndx++;
                    }
                    stmt->insert();
                }
            }
            input_file.close();
            stmt->~statement();
            con->~connection();
            acc->~account();
        }
        /** default destructor */
        ~DataImport() = default;
    protected:
    private:
};
} // namespace vkfencoder
#endif // DATAIMPORT_HPP
