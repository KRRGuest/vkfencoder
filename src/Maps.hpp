#ifndef MAPS_HPP
#define MAPS_HPP

#include <iostream> // for std::cout,std::cerr
#include <cassert> // for assert
#include <algorithm> // for std::for_each

#include <string> // for std::string
//#include <list> // for std::list
#include <boost/dynamic_bitset.hpp>
#include <vector> // for std::vector
#include <utility> // for std::pair, std::move()
#include <map> // for std::map
#include <memory> // for std::unique_ptr

class Maps {
    private:
        const std::string attribute_name;
        std::map<boost::dynamic_bitset<>, std::string> values;
        std::map<std::string,boost::dynamic_bitset<>> encodings;
        std::map<char,boost::dynamic_bitset<>> letter_encodings;
    public:
        /** default constructor */
        Maps() = delete;
        Maps(std::string& part_name, std::string& attribute_value, 
                                char value_letter, 
                                std::size_t num_bits, 
                                unsigned long bitset_ulong) : attribute_name(part_name) {
            AddValue(attribute_value,value_letter, num_bits, bitset_ulong);
        }

        /** Add Value to corresponting Maps
         * \param attribute_value saves the value of the attribute
         * \param value_letter saves the letter for the value ofthe attribute
         * \param num_bits contains length of bitset representation
         * \param bitset_ulong contains binary string encoding
         * \return nothing */
        void AddValue(std::string& attribute_value, char value_letter, std::size_t num_bits, unsigned long bitset_ulong) {
            values.emplace(std::move(boost::dynamic_bitset<>(num_bits, bitset_ulong)), attribute_value);
            encodings.emplace(attribute_value, std::move(boost::dynamic_bitset<>(num_bits, bitset_ulong)));
            letter_encodings.emplace(value_letter, std::move(boost::dynamic_bitset<>(num_bits, bitset_ulong)));
            // std::cout << "To attribute " << getAttributeName() << " the value ";
            // std::cout << attribute_value << " with letter ";
            // std::cout << value_letter << " and the bitset ";
            // std::cout << std::move(boost::dynamic_bitset<>(num_bits, bitset_ulong)) << " is added" << std::endl;
            // std::cout << values.size() << " values.size() ";
            // std::cout << encodings.size() << " encodings.size() ";
            // std::cout << letter_encodings.size() << " letter_encodings.size() ";
        }
        /** Access to the name of encoded attribute
         * \return attribute_name */
        const std::string getAttributeName() const { return attribute_name; }
        /** Seek for a value name encoded by binary string
         * \param buffer contains binary string encoding
         * \return name of a value correponding to the encoding */
        std::string findValueName(const boost::dynamic_bitset<> &buffer) const {
            return values.find(buffer)->second;
        }
        /** Seek for encoding for a value given by a single letter
         * \param letter determines value which encoding is looking for
         * \return binary string encoding */
        boost::dynamic_bitset<> findEncoding(char letter) const {
            std::map<char,boost::dynamic_bitset<>>::const_iterator it = letter_encodings.find(letter);
            if (it != letter_encodings.end()) { 
                return it->second;
            } else {
                return letter_encodings.find('_')->second;
            }
        }
        /** Seek for encoding for a value given by its name
         * \param name determines value which encoding is looking for
         * \return binary string encoding */
        boost::dynamic_bitset<> findEncoding(std::string name) const {
            std::map<std::string,boost::dynamic_bitset<>>::const_iterator it = encodings.find(name);
            if (it != encodings.end()) { 
                return it->second;
            } else {
                return encodings.find("null")->second;
            }
        }
};

typedef std::unique_ptr<Maps> MapsPtr;

#endif // MAPS_HPP
