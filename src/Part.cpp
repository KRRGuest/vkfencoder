#include "Part.hpp"

// 1st step of calculations
Permutation::Permutation(std::vector<Column>&& adjacency){
//    for(auto&& column : adjacency){
//        std::cout << column << std::endl;
//    }
    this->topSort(std::move(adjacency));
}

void Permutation::topSort(std::vector<Column>&& adjacency) {
    std::size_t columns_number=adjacency.size();
//    for(auto&& column : adjacency){
//        std::cout << column << std::endl;
//    }
    for(std::size_t ndx=0; ndx<columns_number; ++ndx) {
        if(!(adjacency[ndx].isDisabled())&&(adjacency[ndx].none())) {
            // save discovered sink of the DAG as the last element of the vector
            this->push_back(ndx);
            // remove the sink of the DAG
            adjacency[ndx].disable();
            // stop a search for the sink
            break;
        }
    }
    if(this->size()!=columns_number) {
        // remove all edges to the last (just removed) sink
        for(auto&& column : adjacency) {
            column.reset(this->back());
        }
        // recurrent call of itself
        this->topSort(std::move(adjacency));
    }
    return;
}

// 2nd step of calculations
Matrix::Matrix(Part* p_part, const Permutation& permutation) {
    std::size_t columns_number=p_part->size();
    assert(columns_number==permutation.size());
    for(std::size_t index=0; index<columns_number; ++index){
        emplace_back(boost::dynamic_bitset<>(columns_number));
    }
    assert(size()==columns_number);
    for(std::size_t ndx=0; ndx<columns_number; ++ndx) {
//        std::cout << permutation[ndx] << std::endl;
        at(permutation[ndx])|=p_part->at(permutation[ndx]);
        at(permutation[ndx]).set(permutation[ndx],true);
        for(std::size_t indx=ndx; indx>0; --indx) {
            if(p_part->at(permutation[ndx])[permutation[indx-1]]){
                at(permutation[ndx])|=at(permutation[indx-1]);
            }
        }
    }
}

// 3rd step of calculations
Part::Obsolete::Obsolete(const Matrix& ordering, const Permutation& permutation) {
    std::size_t columns_number=ordering.size();
    assert(columns_number==permutation.size());
    boost::dynamic_bitset<> buffer;
    for(std::size_t index=0; index<columns_number-2; ++index) {
        bool is_supremum=false;
        for(std::size_t indx=index+1; indx<columns_number-1; ++indx) {
            for(std::size_t ndx=indx+1; ndx<columns_number; ++ndx) {
                buffer=ordering[permutation[indx]];
//                std::cout << buffer << std::endl;
                buffer&=ordering[permutation[ndx]];
//                std::cout << buffer << std::endl;
//                std::cout << ordering[permutation[index]] << std::endl;
                if(ordering[permutation[index]]==buffer){
                    is_supremum=true;
                    break;
                }
            }
            if(is_supremum)
                break;
        }
        this->push_back(is_supremum);
    }
    // enable the column that correponds to some atomic value
    this->push_back(false);
    // disable the column that correponds to the empty value
    this->push_back(true);
}

// all steps together
void Part::ComputeMaps() {
    std::size_t columns_number=this->size();
    // 1st step
    std::vector<Column> temp_adj;
    for(auto&& column : *this){
        temp_adj.push_back(column);
    }
    Permutation top_sort(std::move(temp_adj));
//    for(auto&& ndx : top_sort){
//        std::cout << ndx << std::endl;
//    }
//    for(auto&& column : *this){
//        std::cout << column << std::endl;
//    }
    // 2nd step
    Matrix ordering(this, top_sort);
//    for(auto&& column : ordering){
//        std::cout << column << std::endl;
//    }
    // 3rd step
    Part::Obsolete deleted(ordering, top_sort);
//    std::cout << deleted << std::endl;
    // 4th step
    for(std::size_t ndx=0; ndx<columns_number; ++ndx) {
        boost::dynamic_bitset<> buffer;
        char single_letter=this->at(ndx).getSingleLetter();
//        std::cout << single_letter << std::endl;
        std::string value_name=this->at(ndx).getValueName();
//        std::cout << value_name << std::endl;
        for(std::size_t index=0; index<columns_number; ++index) {
            while(deleted[index]&&index<columns_number){
                ++index;
            }
            if(index<columns_number) {
                buffer.push_back(ordering[top_sort[index]][ndx]);
            }
        }
//        std::cout << buffer << std::endl;
        values.emplace(buffer,std::make_pair(value_name,single_letter));
        encodings.emplace(value_name,buffer);
        letter_encodings.emplace(single_letter,buffer);
    }
    return;
}
