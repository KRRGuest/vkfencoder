#ifndef DATALOAD_HPP
#define DATALOAD_HPP

#define DEFAULT_HOST "localhost"
#define DEFAULT_PASS "toor"
#define DEFAULT_USER "root"

namespace vkfencoder{
class DataLoad {
    public:
        /** default constructor */
        DataLoad() = default;
        /** copy-constructor */
        DataLoad(const DataLoad&) = delete;
        /** move-constructor */
        DataLoad(DataLoad&&) = default;
        /** copy-operator= */
        DataLoad& operator=(const DataLoad& rhs) = delete;
        /** move-operator= */
        DataLoad& operator=(DataLoad&& rhs) = default;
        /** Constructor Sample from file implementation */
        DataLoad(   const std::string &input_file_name,
                    const std::size_t goal_value,
                    const std::string &verges_table_name,
                    const std::string &data_table_name,
                    const std::string &database = DEFAULT_DB,
                    const std::string &separator = ",",
                    const std::string &url = DEFAULT_HOST,
                    const std::string &user = DEFAULT_USER,
                    const std::string &password = DEFAULT_PASS
                ) {
            // set up the account
            mariadb::account_ref acc = mariadb::account::create(url, user, password);
            // create connection
            mariadb::connection_ref con = mariadb::connection::create(acc);
            // create connection
            std::ifstream input_file(input_file_name);
            std::string input_file_line;
            /**< Parse the input file and save it into table encoder of database through connection con */
            con->execute("USE " + database);
            // destroy the table verges, if it exists
            con->execute("DROP TABLE IF EXISTS " + verges_table_name);
            // construct relational structute of the table verges
            std::string sql_query("CREATE TABLE IF NOT EXISTS ");
            sql_query.append(verges_table_name);
            sql_query.append(" (row_id int(8) NOT NULL AUTO_INCREMENT, ");
            sql_query.append("attribute_name varchar(255) NOT NULL DEFAULT '', ");
            sql_query.append("value_verge float(53) NOT NULL DEFAULT 0.E1, ");
            sql_query.append("PRIMARY KEY(row_id)) ");
            sql_query.append("ENGINE = InnoDB");
            con->execute(sql_query);
            // destroy the table training, if it exists
            con->execute("DROP TABLE IF EXISTS " + data_table_name);
            sql_query.clear();
            // construct relational structute of the table verges
            sql_query.append("CREATE TABLE IF NOT EXISTS ");
            sql_query.append(data_table_name);
            sql_query.append(" (row_id int(8) NOT NULL AUTO_INCREMENT, ");
            sql_query.append("real_sign tinyint(1) NOT NULL DEFAULT 1, ");
            std::size_t prev = 0;
            std::size_t pos = std::string::npos;
            if(!std::getline(input_file,input_file_line).eof()) {
                pos = input_file_line.find(separator, prev);
            }
            prev = pos + separator.size(); // ignore the goal attribute
            do {
                pos = input_file_line.find(";", prev);
                if (pos == std::string::npos) {
                    pos = input_file_line.length();
                }
                std::string token = input_file_line.substr(prev, pos-prev);
                if (!token.empty()) {
                    std::string attr_name;
                    std::size_t begin_pos = token.find("\"");
                    begin_pos = begin_pos + 1; // std::string("\"").size() == 1
                    std::size_t stop_pos = token.find("\"", begin_pos);
                    do {
                        std::size_t end_pos = token.find(" ", begin_pos);
                        if (end_pos == std::string::npos) {
                            end_pos = stop_pos;
                        }
                        attr_name.append(token.substr(begin_pos, end_pos-begin_pos));
                        if (end_pos != stop_pos) {
                            attr_name.append("_");
                        }
                        begin_pos = end_pos + 1;  // std::string(" ").size() == 1
                    } while (begin_pos <= stop_pos);
                    sql_query.append(attr_name);
                    sql_query.append(" float(53) NOT NULL DEFAULT 0E0, ");
                }
                prev = pos + separator.size();
            } while (pos < input_file_line.length() && prev < input_file_line.length());
            sql_query.append("PRIMARY KEY(row_id)) ");
            sql_query.append("ENGINE = InnoDB");
            con->execute(sql_query);
            /* insert attributes values into data_table */
            while (!std::getline(input_file,input_file_line).eof()) {
                std::size_t prev = 0;
                std::size_t pos = input_file_line.find(separator, prev);
                if (pos == std::string::npos) {
                    pos = input_file_line.length();
                }
                std::string token = input_file_line.substr(prev, pos-prev);
                sql_query.clear();
                sql_query.append("INSERT INTO " + data_table_name + " VALUES (NULL, '");
                sql_query.append(std::stoi(token) > goal_value ? std::to_string(1) : std::to_string(0));
                prev = pos+1;
                do {
                    pos = input_file_line.find(separator, prev);
                    if (pos == std::string::npos) {
                        pos = input_file_line.length();
                    }
                    std::string token = input_file_line.substr(prev, pos-prev);
                    if (!token.empty()) {
                        sql_query.append("', '");
                        sql_query.append(token);
                    }
                    prev = pos + separator.size();
                } while (pos < input_file_line.length() && prev < input_file_line.length());
                sql_query.append("')");
                con->insert(sql_query);
            }
            input_file.close();
            con->~connection();
            acc->~account();
        }

        /** default destructor */
        ~DataLoad() = default;
    protected:
    private:
};
} // namespace vkfencoder
#endif // DATALOAD_HPP
