#ifndef PART_HPP
#define PART_HPP

#include <iostream> // for std::cout,std::cerr
#include <cassert> // for assert
#include <algorithm> // for std::for_each
#include <string> // for std::string
#include <vector> // for std::vector
#include <utility> // for std::pair, std::move()
#include <map> // for std::map
#include <memory> // for std::unique_ptr

#include <boost/dynamic_bitset.hpp>

class Column : public boost::dynamic_bitset<> {
    private:
        std::string value_name;
        char single_letter;
        bool disabled;
    public:
        /** default constructor */
        Column(std::string name, char letter) : value_name(name), single_letter(letter), disabled(false) {}
        /** Access to value name
         * \return value_name */
        std::string getValueName () { return value_name; }
        /** Access to a single letter for the value
         * \return single_letter */
        char getSingleLetter() { return single_letter; }
        /** Is column disabled?
         * \return answer as bool */
        bool isDisabled() { return disabled; }
        /** Set disabled status */
        void disable() { disabled = true; return; }
};

class Permutation : public std::vector<std::size_t> {
    public:
        /** default constructor */
        Permutation() = default;
        /** main constructor */
        Permutation(std::vector<Column>&& adjacency);
        /** Topological sort vertices of the DAG
         * \param adjacency contains DAG adjacency matrix */
        void topSort(std::vector<Column>&& adjacency);
};

class Part; 

class Matrix : public std::vector<boost::dynamic_bitset<>> {
    friend class Part; /**< Matix declares class Part as its friend class */
    // The private members of class Part can be accessed because Matrix is declared as a friend class of Part too
    public:
        Matrix(Part* p_part, const Permutation& permutation);
};

class Part : public std::vector<Column> {
    friend class Matrix; /**< Part declares class Matrix as its friend class */
    // The private members of class Matix can be accessed because Part is declared as a friend class of Matix too
    private:
        std::string part_name;
        std::map<boost::dynamic_bitset<>, std::pair<std::string,char>> values;
        std::map<std::string,boost::dynamic_bitset<>> encodings;
        std::map<char,boost::dynamic_bitset<>> letter_encodings;
    public:
        class Obsolete : public boost::dynamic_bitset<> {
            public:
                Obsolete(const Matrix& ordering, const Permutation& permutation);
        };
        /** default constructor */
        Part(const std::string &attribute_name) : part_name(attribute_name) {}
        /** Compute maps encoding/decoding */
        void ComputeMaps();
        /** Access to the name of encoded attribute
         * \return part_name */
        std::string getAttributeName() { return part_name; }
        /** Seek for a value name encoded by binary string
         * \param buffer contains binary string encoding
         * \return name of a value correponding to the encoding */
        std::string findValueName(boost::dynamic_bitset<> buffer) {
            return std::get<0>(values.find(buffer)->second);
        }
        /** Seek for a single letter encoded by binary string
         * \param buffer contains binary string encoding
         * \return single letter correponding to the encoding */
        char findSingleLetter(boost::dynamic_bitset<> buffer) {
            return std::get<1>(values.find(buffer)->second);
        }
        /** Seek for encoding for a value given by a single letter
         * \param letter determines value which encoding is looking for
         * \return binary string encoding */
        boost::dynamic_bitset<> findEncoding(char letter) {
            return letter_encodings.find(letter)->second;
        }
        /** Seek for encoding for a value given by its name
         * \param name determines value which encoding is looking for
         * \return binary string encoding */
        boost::dynamic_bitset<> findEncoding(std::string name) {
            return encodings.find(name)->second;
        }
};

#endif // PART_HPP
