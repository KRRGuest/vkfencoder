#ifndef XMLIMPORT_HPP
#define XMLIMPORT_HPP

#define DEFAULT_DB "mushroom"
#define DEFAULT_HOST "localhost"
#define DEFAULT_PASS "toor"
#define DEFAULT_USER "root"

#include <string>
#include <cassert>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include "Part.hpp"

#include <mariadb++/connection.hpp>
#include <mariadb++/statement.hpp>

namespace vkfencoder {
class XMLImport : public std::vector<Part> {
    public:
        /** default constructor */
        XMLImport() = default;
        XMLImport(const std::string &input_file_name,
                    const std::string &encoder_table_name,
                    const std::string &lattices_table_name,
                    const std::string &database = DEFAULT_DB,
                    const std::string &url = DEFAULT_HOST,
                    const std::string &user = DEFAULT_USER,
                    const std::string &password = DEFAULT_PASS
                    ) {
            // set up the account
            mariadb::account_ref acc = mariadb::account::create(url, user, password);
            // create connection
            mariadb::connection_ref con = mariadb::connection::create(acc);
            // create the database, if it does not exist
            con->execute("CREATE DATABASE IF NOT EXISTS " + database + " DEFAULT CHARACTER SET utf8");
            con->execute("USE " + database);
            // destroy the Table encoder, if it exists
            con->execute("DROP TABLE IF EXISTS " + encoder_table_name);
            // costruct relational structute of the Table encoder
            std::string sql_query("CREATE TABLE IF NOT EXISTS " + encoder_table_name);
            sql_query.append(" (row_id int(8) NOT NULL AUTO_INCREMENT, ");
            sql_query.append("attribute_name varchar(255) NOT NULL DEFAULT '', ");
            sql_query.append("attribute_value varchar(255) NOT NULL DEFAULT 'null', ");
            sql_query.append("value_letter char(1) NOT NULL DEFAULT '_', ");
            sql_query.append("value_bitset int(16) NOT NULL DEFAULT 0, ");
            sql_query.append("bitset_length int(8) NOT NULL DEFAULT 1, ");
            sql_query.append("PRIMARY KEY(row_id)) ");
            sql_query.append("ENGINE = InnoDB");
            con->execute(sql_query);
            // destroy the Table orderings, if it exists
            con->execute("DROP TABLE IF EXISTS " + lattices_table_name);
            // costruct relational structute of the Table orderings
            sql_query.clear();
            sql_query.append("CREATE TABLE IF NOT EXISTS " + lattices_table_name);
            sql_query.append(" (row_id int(8) NOT NULL AUTO_INCREMENT, ");
            sql_query.append("attribute_name varchar(255) NOT NULL DEFAULT '', ");
            sql_query.append("source_value varchar(255) NOT NULL DEFAULT '', ");
            sql_query.append("target_value varchar(255) NOT NULL DEFAULT 'null', ");
            sql_query.append("PRIMARY KEY(row_id)) ");
            sql_query.append("ENGINE = InnoDB");
            con->execute(sql_query);
            // parse input file and save data into boost::property_tree
            std::string attribute_name;
            std::string attribute_value;
            std::string value_letter;
            unsigned long value_bitset;
            unsigned int bitset_length;
            mariadb::statement_ref stmt_lattices = con->create_statement("INSERT INTO " + lattices_table_name + " VALUES (NULL, ?, ?, ?)");
            mariadb::statement_ref stmt_encoder = con->create_statement("INSERT INTO " + encoder_table_name + " VALUES (NULL, ?, ?, ?, ?, ?)");
            bool are_graphs_correct = false; // test correctness of graphs' structure
            boost::property_tree::ptree buffer_ptree;
            boost::property_tree::read_xml(input_file_name, buffer_ptree);
            /**< Traverse property tree buffer_ptree */
            // Structure of the XML file:
            // - - Element "attribute"  child of the root
            // - - - Element "vertices" child of the attribute Element
            // - - - - Element "node"   child of the vertices Element
            // - - - Element "edges"    child of the attribute Element
            // - - - - Element "arc"    child of the edges Element
            std::string title = buffer_ptree.get<std::string>("<xmlattr>.name", database);
            for (auto node : buffer_ptree.get_child("document")) {
                if (node.first == "attribute") {
                    boost::property_tree::ptree subtree = node.second;
                    attribute_name = subtree.get<std::string>("<xmlattr>.name", "garbage");
                    Part&& part(attribute_name);
                    std::map<std::string,std::size_t> detect_column;
                    std::size_t index = 0;
                    for (auto node2 : subtree.get_child("")) {
                        if (node2.first == "vertices") {
                            for (auto node3 : node2.second.get_child("")) {
                                boost::property_tree::ptree vertices = node3.second;
                                detect_column.emplace(vertices.get<std::string>("<xmlattr>.string", "null"), index);
                                part.emplace_back(Column(vertices.get<std::string>("<xmlattr>.string", "null"),
                                                            vertices.get<std::string>("<xmlattr>.char", "_")[0]));
                                index++;
                            }
                        }
                    }
                    std::size_t length=detect_column.size();
                    assert(part.size()==length);
                    for(auto&& column : part){
                        column.resize(length);
                    }
                    are_graphs_correct = true;
                    for (auto node2 : subtree.get_child("")) {
                        if (node2.first == "edges") {
                            for (auto node3 : node2.second.get_child("")) {
                                boost::property_tree::ptree edges = node3.second;
                                std::string starget = edges.get<std::string>("<xmlattr>.target", "null");
                                auto it_target = detect_column.find(starget);
                                std::string ssource = edges.get<std::string>("<xmlattr>.source", "null");
                                auto it_source = detect_column.find(ssource);
                                if (it_target == detect_column.end() || it_source == detect_column.end()) {
                                    are_graphs_correct = false;
                                    break;
                                } else {
                                    part[it_target->second].set(it_source->second,true);
                                    part[it_target->second].set(it_source->second,true);
                                    stmt_lattices->set_string(0, attribute_name);
                                    stmt_lattices->set_string(1, ssource);
                                    stmt_lattices->set_string(2, starget);
                                    stmt_lattices->insert();
                                }
                            }
                            if (are_graphs_correct) {
                                part.ComputeMaps();
                                for(auto column : part) {
                                    attribute_value = column.getValueName();
                                    value_letter = column.getSingleLetter();
                                    boost::dynamic_bitset<> bdbs = part.findEncoding(attribute_value);
                                    value_bitset = bdbs.to_ulong();
                                    bitset_length = bdbs.size();
                                    stmt_encoder->set_string(0, attribute_name);
                                    stmt_encoder->set_string(1, attribute_value);
                                    stmt_encoder->set_string(2, value_letter);
                                    stmt_encoder->set_unsigned32(3, value_bitset);
                                    stmt_encoder->set_unsigned32(4, bitset_length);
                                    stmt_encoder->insert();
                                }
                            }
                        }
                    }
                }
            }
            if (!are_graphs_correct) {
                con->execute("USE " + database);
                con->execute("DROP TABLE IF EXISTS " + encoder_table_name);
                con->execute("DROP TABLE IF EXISTS " + lattices_table_name);
                throw std::runtime_error("Errors in graphs structures");
            }
            stmt_lattices->~statement();
            stmt_encoder->~statement();
            con->~connection();
            acc->~account();
        }
        /** default destructor */
        ~XMLImport()=default;

    private:
};
} // namespace vkfencoder
#endif // XMLIMPORT_HPP
