#ifndef PAIRVECTORS_HPP
#define PAIRVECTORS_HPP

#include <iostream> // for std::cout,std::cerr
#include <cassert> // for assert
#include <algorithm> // for std::for_each

#include <string> // for std::string
//#include <list> // for std::list
#include <boost/dynamic_bitset.hpp>
#include <vector> // for std::vector
#include <utility> // for std::pair, std::move()
#include <memory> // for std::unique_ptr

class PairVectors {
    private:
        const std::string attribute_name;
    public:
        std::vector<std::pair<std::string, char>> values;
        std::vector<std::pair<std::string,std::string>> order;

        /** default constructor */
        PairVectors() = default;
        PairVectors(const std::string& part_name) : attribute_name(part_name) { }

        /** Add Value to corresponting Maps
         * \param attribute_value saves the value of the attribute
         * \param value_letter saves the letter for the value ofthe attribute
         * \return nothing */
        void AddValue(std::string&& attribute_value, char value_letter) {
            values.emplace_back(std::make_pair(std::move(attribute_value), value_letter));
        }
        /** Add Order to corresponting Maps
         * \param source_value saves the origin of the arc
         * \param target_value saves the end pf the arc
         * \return nothing */
        void AddOrder(std::string&& source_value, std::string&& target_value) {
            order.emplace_back(std::make_pair(std::move(source_value), std::move(target_value)));
        }
        /** Access to the name of encoded attribute
         * \return attribute_name */
        const std::string getAttributeName() const { return attribute_name; }
};

#endif // PAIRVECTORS_HPP
