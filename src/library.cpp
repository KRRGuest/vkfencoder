#include <pybind11/pybind11.h>
#include "JSONImport.hpp"
#include "JSONExport.hpp"
#include "XMLImport.hpp"
#include "XMLExport.hpp"
#include "DataImport.hpp"
#include "DataLoad.hpp"

namespace py = pybind11;

PYBIND11_MODULE(vkfencoder, m) {
    py::class_<vkfencoder::DataImport>(m, "DataImport")
        .def(py::init<std::string &, char &, std::string &, std::string &>(),
                    "Construct DataImport (4 args)",
                    py::arg("input_file_name"), py::arg("goal_value"), py::arg("encoder_table_name"), py::arg("data_table_name"))
        .def(py::init<std::string &, char &, std::string &, std::string &, std::string &>(),
                    "Construct DataImport (5 args)",
                    py::arg("input_file_name"), py::arg("goal_value"), py::arg("encoder_table_name"), py::arg("data_table_name"),
                    py::arg("database"))
        .def(py::init<std::string &, char &, std::string &, std::string &, std::string &, std::string &>(),
                    "Construct DataImport (6 args)",
                    py::arg("input_file_name"), py::arg("goal_value"), py::arg("encoder_table_name"), py::arg("data_table_name"),
                    py::arg("database"), py::arg("url"))
        .def(py::init<std::string &, char &, std::string &, std::string &, std::string &, std::string &, std::string &>(),
                    "Construct DataImport (7 args)",
                    py::arg("input_file_name"), py::arg("goal_value"), py::arg("encoder_table_name"), py::arg("data_table_name"),
                    py::arg("database"), py::arg("url"), py::arg("user"))
        .def(py::init<std::string &, char &, std::string &, std::string &, std::string &, std::string &, std::string &, std::string &>(),
                    "Construct DataImport (8 args)",
                    py::arg("input_file_name"), py::arg("goal_value"), py::arg("encoder_table_name"), py::arg("data_table_name"),
                    py::arg("database"), py::arg("url"), py::arg("user"), py::arg("pass"))
    ;
    py::class_<vkfencoder::DataLoad>(m, "DataLoad")
        .def(py::init<std::string &, std::size_t, std::string &, std::string &, std::string &>(), "Construct DataImport (5 args)",
                    py::arg("input_file_name"), py::arg("goal_value"), py::arg("verges_table_name"), py::arg("data_table_name"),
                    py::arg("database"))
        .def(py::init<std::string &, std::size_t, std::string &, std::string &, std::string &, std::string &>(),
                    "Construct DataImport (6 args)",
                    py::arg("input_file_name"), py::arg("goal_value"), py::arg("verges_table_name"), py::arg("data_table_name"),
                    py::arg("database"), py::arg("separator"))
        .def(py::init<std::string &, std::size_t, std::string &, std::string &, std::string &, std::string &,
                    std::string &>(), "Construct DataImport (7 args)",
                    py::arg("input_file_name"), py::arg("goal_value"), py::arg("verges_table_name"), py::arg("data_table_name"),
                    py::arg("database"), py::arg("separator"), py::arg("url"))
        .def(py::init<std::string &, std::size_t, std::string &, std::string &, std::string &, std::string &,
                    std::string &, std::string &>(), "Construct DataImport (8 args)",
                    py::arg("input_file_name"), py::arg("goal_value"), py::arg("verges_table_name"), py::arg("data_table_name"),
                    py::arg("database"), py::arg("separator"), py::arg("url"), py::arg("user"))
        .def(py::init<std::string &, std::size_t, std::string &, std::string &, std::string &, std::string &,
                    std::string &, std::string &, std::string &>(), "Construct DataImport (9 args)",
                    py::arg("input_file_name"), py::arg("goal_value"), py::arg("verges_table_name"), py::arg("data_table_name"),
                    py::arg("database"), py::arg("separator"), py::arg("url"), py::arg("user"), py::arg("pass"))
    ;
    py::class_<vkfencoder::JSONExport>(m, "JSONExport")
        .def(py::init<std::string &, std::string &, std::string &>(),
                    "Construct JSONExport (3 args)",
                    py::arg("output_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"))
        .def(py::init<std::string &, std::string &, std::string &, std::string &>(),
                    "Construct JSONExport (4 args)",
                    py::arg("output_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"),
                    py::arg("database"))
        .def(py::init<std::string &, std::string &, std::string &, std::string &, std::string &>(),
                    "Construct JSONExport (5 args)",
                    py::arg("output_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"),
                    py::arg("database"), py::arg("url"))
        .def(py::init<std::string &, std::string &, std::string &, std::string &, std::string &, std::string &>(),
                    "Construct JSONExport (6 args)",
                    py::arg("output_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"),
                    py::arg("database"), py::arg("url"), py::arg("user"))
        .def(py::init<std::string &, std::string &, std::string &, std::string &, std::string &, std::string &, std::string &>(),
                    "Construct JSONExport (7 args)",
                    py::arg("output_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"),
                    py::arg("database"), py::arg("url"), py::arg("user"), py::arg("pass"))
    ;
    py::class_<vkfencoder::JSONImport>(m, "JSONImport")
        .def(py::init<std::string &, std::string &, std::string &>(),
                    "Construct JSONImport (3 args)",
                    py::arg("input_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"))
        .def(py::init<std::string &, std::string &, std::string &, std::string &>(),
                    "Construct JSONImport (4 args)",
                    py::arg("input_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"),
                    py::arg("database"))
        .def(py::init<std::string &, std::string &, std::string &, std::string &, std::string &>(),
                    "Construct JSONImport (5 args)",
                    py::arg("input_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"),
                    py::arg("database"), py::arg("url"))
        .def(py::init<std::string &, std::string &, std::string &, std::string &, std::string &, std::string &>(),
                    "Construct JSONImport (6 args)",
                    py::arg("input_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"),
                    py::arg("database"), py::arg("url"), py::arg("user"))
        .def(py::init<std::string &, std::string &, std::string &, std::string &, std::string &, std::string &, std::string &>(),
                    "Construct JSONImport (7 args)",
                    py::arg("input_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"),
                    py::arg("database"), py::arg("url"), py::arg("user"), py::arg("pass"))
    ;
    py::class_<vkfencoder::XMLExport>(m, "XMLExport")
        .def(py::init<std::string &, std::string &, std::string &>(),
                    "Construct XMLExport (3 args)",
                    py::arg("output_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"))
        .def(py::init<std::string &, std::string &, std::string &, std::string &>(),
                    "Construct XMLExport (4 args)",
                    py::arg("output_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"),
                    py::arg("database"))
        .def(py::init<std::string &, std::string &, std::string &, std::string &, std::string &>(),
                    "Construct XMLExport (5 args)",
                    py::arg("output_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"),
                    py::arg("database"), py::arg("url"))
        .def(py::init<std::string &, std::string &, std::string &, std::string &, std::string &, std::string &>(),
                    "Construct XMLExport (6 args)",
                    py::arg("output_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"),
                    py::arg("database"), py::arg("url"), py::arg("user"))
        .def(py::init<std::string &, std::string &, std::string &, std::string &, std::string &, std::string &, std::string &>(),
                    "Construct XMLExport (7 args)",
                    py::arg("output_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"),
                    py::arg("database"), py::arg("url"), py::arg("user"), py::arg("pass"))
    ;
    py::class_<vkfencoder::XMLImport>(m, "XMLImport")
        .def(py::init<std::string &, std::string &, std::string &>(),
                    "Construct XMLImport (3 args)",
                    py::arg("input_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"))
        .def(py::init<std::string &, std::string &, std::string &, std::string &>(),
                    "Construct XMLImport (4 args)",
                    py::arg("input_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"),
                    py::arg("database"))
        .def(py::init<std::string &, std::string &, std::string &, std::string &, std::string &>(),
                    "Construct XMLImport (5 args)",
                    py::arg("input_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"),
                    py::arg("database"), py::arg("url"))
        .def(py::init<std::string &, std::string &, std::string &, std::string &, std::string &, std::string &>(),
                    "Construct XMLImport (6 args)",
                    py::arg("input_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"),
                    py::arg("database"), py::arg("url"), py::arg("user"))
        .def(py::init<std::string &, std::string &, std::string &, std::string &, std::string &, std::string &, std::string &>(),
                    "Construct XMLImport (7 args)",
                    py::arg("input_file_name"), py::arg("encoder_table_name"), py::arg("lattices_table_name"),
                    py::arg("database"), py::arg("url"), py::arg("user"), py::arg("pass"))
    ;
}
