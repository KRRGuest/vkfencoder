#ifndef JSONEXPORT_HPP
#define JSONEXPORT_HPP

#define DEFAULT_DB "mushroom"
#define DEFAULT_HOST "localhost"
#define DEFAULT_PASS "toor"
#define DEFAULT_USER "root"

#include <mariadb++/account.hpp> 
#include <mariadb++/connection.hpp>

#include <string>
#include <cassert>
#include <map>
#include <vector>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/optional/optional.hpp>

#include "PairVectors.hpp"

namespace vkfencoder{
class JSONExport : public std::vector<PairVectors> {
    public:
    /** default constructor */
    JSONExport() = default;
    JSONExport(const std::string &output_file_name, 
                const std::string &encoder_table_name, 
                const std::string &lattices_table_name,
                const std::string &database = DEFAULT_DB,
                const std::string &url = DEFAULT_HOST,
                const std::string &user = DEFAULT_USER, 
                const std::string &password = DEFAULT_PASS
                ) {
        // Create an empty property tree object.
        boost::property_tree::ptree tree;
        // set up the account
        mariadb::account_ref acc = mariadb::account::create(url, user, password);
        // create connection
        mariadb::connection_ref con = mariadb::connection::create(acc);
        // open database 
        con->execute("USE " + database);
        // obtain content of table encoder 
        mariadb::result_set_ref result = con->query("SELECT * FROM " + encoder_table_name);
        // reading from a result set
        std::string part_name; /**< This variable contains the name of an attribute in current row */
        std::string attribute_value; /**< This variable saves the value in current row */
//        char value_letter; /**< This variable saves the letter in current row */
        std::vector<std::string> used_attributes;
        while (result->next()) {
            part_name = result->get_string("attribute_name");
            if((std::find(used_attributes.begin(), used_attributes.end(), part_name)) == used_attributes.end()) {
                used_attributes.emplace_back(part_name);
                this->emplace_back(part_name);
                this->back().AddValue(result->get_string("attribute_value"), result->get_string("value_letter")[0]);
            } else /* if attribute exists */ {
                std::size_t index = this->findIndex(part_name);
                this->at(index).AddValue(result->get_string("attribute_value"), result->get_string("value_letter")[0]);
            }           
        }
        // obtain content of table orderings 
        result = con->query("SELECT * FROM " + lattices_table_name);
        // reading from a result set
        while (result->next()) {
            part_name = result->get_string("attribute_name");
            if ((std::find(used_attributes.begin(), used_attributes.end(), part_name)) == used_attributes.end()) {   // if yes
                throw std::runtime_error("Ordering between values of an nonsexistent attribute!");
            } else /* if attribute exists */ {
                std::size_t index = this->findIndex(part_name);
                this->at(index).AddOrder(result->get_string("source_value"), result->get_string("target_value"));
            }
        }
        tree.add("document", "").add("<xmlattr>.name", database);
        auto &subtree = tree.get_child("document");
        for ( std::vector<PairVectors>::iterator it = this->begin(); it != this->end(); it++) {
            part_name = it->getAttributeName();
            boost::property_tree::ptree lattice;
            lattice.add("attribute", "").add("<xmlattr>.name", part_name);
            boost::property_tree::ptree vertices;
            auto &items = lattice.get_child("attribute");
            for (auto &vertex : it->values) {
                auto &temp = vertices.add("node", "");
                temp.add("<xmlattr>.string", vertex.first);
                temp.add("<xmlattr>.char", vertex.second);
            }
            items.add_child("vertices", vertices);
            boost::property_tree::ptree edges;
            for (auto &arc : it->order) {
                auto &temp = edges.add("arc", "");
                temp.add("<xmlattr>.source", arc.first);
                temp.add("<xmlattr>.target", arc.second);
            }
            items.add_child("edges", edges);
            subtree.add_child("attribute", items);
        }
        boost::property_tree::write_json(output_file_name, tree);
        con->~connection();
        acc->~account();
    }
    private:
        std::size_t findIndex(std::string &part_name) {
            std::size_t index = this->size();
            for (std::size_t ndx = 0; ndx < index; ndx++) {
                if (this->at(ndx).getAttributeName() == part_name) {
                    index = ndx;
                    break;
                }
            }
            return index;
        }
};
} // namespace vkfencoder
#endif // JSONEXPORT_HPP
